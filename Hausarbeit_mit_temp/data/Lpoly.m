function [fxn] = Lpoly(x,xsamples,fsamples,N)
%Lpoly berechnet ein Lagranges Interpolationspolynom fuer gegebene Stuetzstellen
% und wertet es and den Stellen x aus

%%%Rueckgabewert wird Initializiert
fxn = x.*0;
    
%%%Falls der Polynomgrad N groesser als die Anzahl der Stuetstellen + 1 ist 
% wird ein Fehler ausgegeben
if N > length(fsamples)-1
    disp('Sorry you need more sample points')
    return
end

for idx = 0:N
    %%%Initializiere Li
    Li = 0*x +1;
    for jdx = 0:N
        if idx ~= jdx
            xi = xsamples(idx+1);
            xj = xsamples(jdx+1);
            Lnext = (x-xj)/(xi-xj);
            Li = Li.*Lnext;
        end
    end
    fxn = fxn + Li*fsamples(idx+1);
end

end

