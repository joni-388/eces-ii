function cSpline(x,y)
fig = figure();
    set(fig,'color','white')
    set(gca,'FontSize',18)
    points = plot(x,y,'r*');
    grid on
    hold on
xx=-20:.1:20;
yy=csapi(x,y,xx);
spl = plot(xx,yy,'b','LineWidth',2);
p=polyfit(x,y,6);
x1=(-20:.1:20);
y1=polyval(p,x1);
poly = plot(x1,y1,'g','Linewidth',2);
legend([points,spl,poly], 'Datenpunkte','Kubischer Spline','Interpolationspolynom')