function [m,b] = myLinearPolyfit(x,y)
b  = calculateB(x,y);
m = calculateM(x,y); 
end

