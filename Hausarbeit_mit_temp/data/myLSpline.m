function myLSpline(X,Y)
    fig = figure();
    set(fig,'color','white')
    set(gca,'FontSize',18)
    points = plot(X,Y,'r*');
    grid on
    hold on

    for i = 1:length(X)-1
        m = (Y(i+1)-Y(i) )/(X(i+1)-X(i));
        xspline = linspace(X(i),X(i+1),10);
        yspline = m*(xspline-X(i)) + Y(i);
        lins = plot(xspline,yspline,'b-','LineWidth',2)
    legend([points, lins], 'Datenpunkte','Linearer Spline')
    
