

\section{Interpolation}\label{sec:interpolation}
	In der Praxis kommt es vor, dass man eine Funktion $f(x)$ nur über ihre $\textit{n}+1$ Wertepaare $ (x_{\textit{i}},f(x_{\textit{i}}))$ mit $ \textit{i}=0,\dots,\textit{n}$ kennt. Diese können zum Beispiel bei einem Versuch erhoben werden. Die sich unterscheidenden $x_{\textit{i}}$ nennt man Knoten und die entsprechenden Wertepaare werden Stützstellen genannt. Um Aussagen treffen zu können wie sich die Funktion $f(x)$ neben den Stützstellen verhält, approximieren wir $f(x)$ mit einem Polynom vom  Grad $n$ und nennen diese Funktion $\tilde{f}(x)$. 
	Die Definition der Interpolation besagt, dass $\tilde{f}(x)$  durch die Stützstellen verlaufen muss, also die Gleichung
	\begin{equation}
	\tilde{f}(x_{\textit{i}}) = f(x_{\textit{i}}) \quad \textit{i}=0,\dots,\textit{n}
	\label{eq:InterPolBed}
	\end{equation}	   
	erfüllen muss. Diese Bedingung wird Interpolationsbedingung genannt und die Funktion $\tilde{f}(x)$, welche die Gleichung (\ref{eq:InterPolBed}) erfüllt, heißt Interpolator von ${f}$.
	Anstatt eines polynomialen Interpolators könnte man auch verschiedene andere Arten wählen, zum Beispiel einen trigonometrischen oder rationalen Interpolator. Im Folgenden betrachten wir nur lineare Interpolatoren $\Phi(x)$
	\begin{equation}
	\Phi(x;a_0,...,a_n)= a_0\Phi_0(x)+a_1\Phi_1(x)+...+a_n\Phi_n(x),
		\label{eq:phi_lin}
	\end{equation}
	welche linear von $n+1 $ Koeffizienten $a_i$ mit $i=0,\dots,n$ abhängen. Dazu gehören die polynomialen und trigonometrischen Interpolatoren, nicht aber die rationalen. 
	Zuerst betrachten wir polynomiale Interpolatoren $P_n(x)$
	\begin{equation}
		P_n(x)=P_n(x;a_1,...,a_n)= a_0 + a_1x+...+a_nx^n,
		\label{eq:P_n}
	\end{equation}
	wobei $n$ den Grad des Polynoms angibt, vgl. \cite[67]{Quarteroni.2006}. In Abbildung \ref{fig:ansch_exp} sieht man beispielhaft, wie Daten mit einem polynomialen Interpolator vom Grad sechs approximiert werden.
	
	\begin{figure}[h!]
		\centering
		\begin{tikzpicture}
		\begin{axis}[xlabel={x}, ylabel = {f(x)}, width =.45\textwidth, height=.45\textwidth, ylabel near ticks, xlabel near ticks, xmin=0, xmax=6,legend style={at={(0.5,-0.15)},anchor=north}]
		\addplot[black, no marks, very thick] table [x=xp, y=yp, col sep = comma]{data/anschaulich_exp_xp.csv};
	
		\addplot[only marks,mark=*,mark options={scale=2, fill=red},text mark as node=true] table [x=x,
		y=y, col sep = comma]{data/anschaulich_exp_x.csv};
		%,mark=*,mark options={scale=2, fill=red},text mark as node=true
		
		\legend{Interpolator $\tilde{f}(x)=P_6(x)$ , Daten}
		\end{axis}
		\end{tikzpicture}
		\caption{Daten mit einem Polynom 6. Grades interpoliert. }
		\label{fig:ansch_exp}
	\end{figure}
	
	%%%%%%%%HIer soll noch ein Bild zur veranschaulichung von Interpolation hin %%%%%%%%%%%
	
	\subsection{Lagrange-Formel}\label{sec:lagr}
	Sei $P_n(x)$ ein Polynom vom Grad kleiner gleich $n$ und $P_n(x)$ erfülle die Interpolationsbedingung (\ref{eq:InterPolBed}) für Stützstellen einer stetigen Funktion $f$, so ist dieses Polynom eindeutig. 
	Den Beweis kann man durch Widerspruch führen. Wir nehmen an, es gäbe zwei verschiedene Polynome $P_n(x)$ und $Q_n(x)$ vom Grad kleiner gleich $ n$,   welche beide (\ref{eq:InterPolBed})  erfüllen. Die Differenz $P_n(x)- Q_n(x)$ ergibt wieder ein Polynom vom Grad kleiner gleich $  n$ mit $n+1$ Nullstellen. Aus dem Fundamentalsatz der Algebra folgt, dass dieses Polynom identisch null sein muss.
	Also folgt $P_n(x) \equiv Q_n(x)$\cite[68]{Quarteroni.2006}.
	\newline \newline
	Um $P_n(x)$ als Polynom schreiben zu können, ist es hilfreich zuerst eine andere Funktion $L_k(x)$ zu betrachten, deren Funktionswert genau dann eins annimmt, wenn $x_i = x_k$ für ein $k$ aus null bis $n$. Diese Eigenschaft entspricht der des Kronecker-Deltas. Sie ist gegeben durch
	\begin{equation}
	L_k(x_i)=\delta_{\textit{ik}} = 
	\begin{cases}	
	1 & \text{falls} \, i = k \\
	0 & \text{falls} \, i \neq k.
	\end{cases} 
	\end{equation}
	Genau dieses Verhalten zeigt ein Polynom, welches man angeben kann als
	\begin{equation}
	L_k(x) =	\prod_{\substack{i=0 \\ i \neq k }}^{n} \frac{x-x_i}{x_k-x_i}, k=0,\dots,n.
	\label{eq:L_k}
	\end{equation}
	Dieses Polynom nimmt für jeden Knoten den Wert eins an. Multipliziert man diesen Term noch mit $y_i$ und summiert von $i=0$ bis $ i=n$, so erfüllt der Term die Interpolationsbedingung (\ref{eq:InterPolBed}). Also erhalten wir für $P_n(x)$ die Darstellung
	\begin{equation}
	P_n(x)= \sum_{k=0}^{n}y_k L_k(x)= \sum_{k=0}^{n}y_k \prod_{ \substack{i=0 \\ i \neq k }}^{n} \frac{x-x_i}{x_k-x_i}, k=0,\dots,n.
	\label{eq:Lagrange_formel}
	\end{equation}
	Diese Darstellung wird Lagrangesche-Darstellung des Interpolationspolynoms genannt. Die Funktionen {$L_k$} spielen eine große Rolle und werden deshalb charakteristische Polynome genannt,
	vgl. \cite[69]{Quarteroni.2006}. 
	
	
	\subsection{Newtonsche Interpolationsformel}
	Um die Koeffizienten $a_i$ des Interpolationspolynoms $P_n(x)$, siehe \eqref{eq:P_n}, zu berechnen, eignet sich in der Praxis oft der Newtonsche Algorithmus. Nach Newton wählt man den Ansatz
	\begin{equation}
	P_n(x)= \gamma_0 + \sum_{i=1}^{n} \gamma_i (x -x_0)... (x-x_{i-1}), \quad \gamma_i = f_{[
		x_0,...,x_i]}.
	\end{equation}
	Diesen setzt man in die Interpolationsbedingung \eqref{eq:InterPolBed} ein, um die $\gamma$-Werte zu berechnen. Aus den erhaltenen Gleichungen lässt sich eine Rekursionsformel 
	\begin{align}
	i=0,...,n:& \quad f_{[x_i]}=y_i \\
	k = 1,...,n: \quad i=0,...,n-k:& \quad
	f_{[x_i,...,x_{i+k}]}= \frac 
	{f_{[x_{i+1},...,x_{i+k}]}-f_{[x_i,...,x_{i+k-1}]}}
	{x_{i+k}-x_i}
	\end{align}
	für die sogenannten dividierten Differenzen $f_{[x_i,...,x_{i+k}]}$ ableiten. Mithilfe der $i$-ten dividierten Differenzen erhält man dann die Parameter $\gamma_i := f_{[x_0,...,x_{i}]}$.
	Ein Beispiel verdeutlicht das Schema der Rekursionsformel.
	Gegeben seien die Stützstellen $(3,1),(1,2),(2,0)$. 
	Wir schreiben die Werte in eine Tabelle
%	\begin{center}
%		\begin{tabular}{l|c c c}
%			$x_0$ &   $f_0$ \\ 			% $\searrow$ 
%			&  	& 		$f_{01}$  \\
%			$x_1$ &  $f_1$ &  &	 $f_{012}$ \\
%			&     & 		$f_{12}$ \\
%			$x_2$ &   $f_2$ 		  \\
%		\end{tabular}
%	\end{center}
	\begin{center}
		$
		\begin{array}{l@{\ }|c c c}
		\setlength{\jot}{20pt}
		x_0=3 & \textcolor{TUDa-2c}{f_{[x_0]}=1} \raisebox{-1ex}{$\searrow$} & &  \\
		& 	  &  \textcolor{TUDa-2c}{f_{[x_0,x_1]}=\frac{2-1}{1-3}=-\frac{1}{2}} \raisebox{-1ex}{$\searrow$} & \\
		x_1=1 & f_{[x_1]}=2 \raisebox{1ex}{$\nearrow$} 			 &  &  \textcolor{TUDa-2c}{f_{[x_0,x_1,x_2]}= \frac{-2+\frac{1}{2}}{2-3}=\frac{3}{2}}\\
		&   \quad \qquad 	\raisebox{1ex}{$\searrow$}  & f_{[x_1,x_2]} = \frac{0-2}{2-1}=-2 \raisebox{1ex}{$\nearrow$}  &  \\
		x_2 =2 & f_{[x_2]}=0 \raisebox{1ex}{$\nearrow$} & &	
		\end{array}
		$	
	\end{center}
	und berechnen die dividierten Differenzen. In der ersten Zeile kann man nun die hervorgehobenen Werte für die $i$-ten dividierten Differenzen, also die Parameter $\gamma_i$ ablesen. Es ergibt sich das Interpolationspolynom $P_2(x)$ zu
	\begin{equation}
		P_2(x)=\textcolor{TUDa-2c}{1} \textcolor{TUDa-2c}{-\frac{1}{2}} (x -x_0) + \textcolor{TUDa-2c}{\frac{3}{2}}(x-x_0)(x-x_1).
	\end{equation}
	Die berechneten dividierten Differenzen kann man speichern und damit später nutzen, um weitere $\gamma$-Werte für eventuell hinzukommende Stützstellen auszurechnen. Das resultierende Polynom ist aufgrund der Eindeutigkeit des Polynoms, dasselbe wie mit Hilfe der Lagrangschen-Darstellung, 
	vgl. \cite[48]{Stoer.2005}. 
	
	\subsection{Fehlerabschätzung}
	Mit der Lagrangeschen-Formel lässt sich auch eine Fehlerabschätzung angeben, wenn folgende Voraussetzung erfüllt ist: $f(x)$ sei auf einem beschränkten Intervall $I$, mit $n+1$ verschiedenen Stützstellen, $n+1$ mal stetig differenzierbar.
	Dann gibt es genau ein $\xi$ in dem Intervall $I$ für den der Fehler $E_nf(x)$ mit
	\begin{equation}
	E_nf(x) = f(x) - P_n(x)= \frac{f^{(n+1)}(\xi)}{(n+1)!} \omega(x) 
	\end{equation}
	angegeben werden kann. Wobei $\omega$
	\begin{equation}
	\omega(x)=\prod_{i=0}^{n}(x-x_i)
	\end{equation}
	das Knotenpolynom bezeichnet \cite[S.53]{Stoer.2005}. Liegen die Knoten gleichmäßig auseinander, d.h. mit einer konstanten Schrittweite $h$, so kann man zeigen, dass
	\begin{equation}
	\Bigl| \prod_{i=0}^{n}(x-x_i) \Bigl| \leq n! \frac{h^{n+1}}{4}
	\end{equation}
	gilt und daraus kann man wiederum schließen, dass
	\begin{equation}
	\max_{x \in I} | E_nf(x)| \leq \frac{ \max_{x \in I} |f^{(n+1)}(x)|}{4(n+1)}h^{n+1}
	\label{eq:FehLagr}
	\end{equation}
	gilt für alle $x$ in dem Intervall $I$ \cite[71]{Quarteroni.2006}. Leider kann man aus (\ref{eq:FehLagr}) nicht schließen, dass $E_nf(x)$ für $n\rightarrow \infty$ gegen null geht. Dies ist zwar für einige Funktionen gegeben, aber nicht für alle. Ein bekanntes Gegenbeispiel ist die Runge-Funktion, siehe Abschnitt \ref*{sec:runges}.
	
	\subsection{Runges Phänomen}\label{sec:runges}
	Charakteristisch für das Verhalten von Polynomen ist, dass sich für $x \rightarrow \infty$ auch ihre Funktionswerte ins positive oder negative Unendliche entwickeln. Versucht man nun eine Funktion, die kein polynomiales Verhalten aufweist, mit einem Polynom zu approximieren, so erhält man innerhalb des betrachteten Intervalls recht gute Ergebnisse. Verwendet man die approximierte Funktion jedoch zum Extrapolieren, wird der Fehler schnell sehr groß. Aber auch schon innerhalb des Intervalls kann es unter bestimmten Voraussetzungen zu großen Abweichungen kommen. Anhand der Runge-Funktion
	\begin{equation}
	R(x)= \frac{1}{1+25x^2} \quad ,x \in [-1;1]
	\label{eq:Runge}
	\end{equation}
	wird dieses Verhalten deutlich. Approximiert man die Runge-Funktion mit einem Interpolationspolynom fünften und neunten Grades, erhält man einen Graphen wie in Abbildung \ref{fig:runge_lagrange}. Der Graph \ref{fig:runge_lag_fehler} zeigt den entsprechenden Fehler $E_5f(x)$ und $E_9f(x)$ zu den Approximationen $P_5(x)$ und $P_9(x)$. Es ist zu erkennen, dass mit steigendem $n$ der Fehler der Approximation in der Mitte des Intervalls immer kleiner wird. In der Nähe der Intervallgrenzen treten jedoch starke Oszillationen auf und der Fehler geht mit $n \rightarrow \infty$ gegen unendlich.
	
	\begin{figure}[h!]
		\centering
		\begin{subfigure}{.45\textwidth}
			\centering
			\begin{tikzpicture}
			\begin{axis}[xlabel={$x$}, ylabel = {$f(x)$}, width =\textwidth, height=.8\textwidth, ylabel near ticks, xlabel near ticks, xmin=-1, xmax=1,legend style={at={(0.5,-0.1)},anchor=north}]
			\addplot[blue, no marks, very thick] table [x=x, y=runge, col sep = comma]{data/Runge_fxn5_fxn9.csv};
			\addplot[red, no marks, very thick] table [x=x, y=fxn_5, col sep = comma]{data/Runge_fxn5_fxn9.csv};
			\addplot[color =black!30!green, no marks, very thick] table [x=x, y=fxn_9, col sep = comma]{data/Runge_fxn5_fxn9.csv};
			\addplot[only marks,mark=*,mark options={scale=1, fill=red},text mark as node=true] table [x=xsamples_5, y=fsamples_5, col sep = comma]{data/Runge_pt5.csv};
			\addplot[only marks,mark=*,mark options={scale=1, fill=black!30!green},text mark as node=true] table [x=xsamples_9, y=fsamples_9, col sep = comma]{data/Runge_pt9.csv};
			%				\addplot[red, ,points,no marks, very thick] table [x=x, y=, col sep = comma]{Runge_pt9.csv};
			\legend{Runge Funktion, Lagrange  $n =5$,Lagrange  $n = 9$, Stützstellen $n =5$, Stützstellen $n =9$}
			
			\end{axis}
			\end{tikzpicture}
			\caption{Runge-Funktion mit Lagrange interpoliert.}
			\label{fig:runge_lagrange}
		\end{subfigure}
		\begin{subfigure}{.45\textwidth}
			\centering
			\begin{tikzpicture}
			\begin{axis}[xlabel={$x$}, ylabel = {$f(x)$},, width =\textwidth, height=.8\textwidth, ylabel near ticks, xlabel near ticks, xmin=-1, xmax=1,legend style={at={(0.5,-0.1)},anchor=north}]
			\addplot[red, no marks, very thick] table [x=x, y=fehler_fxn5, col sep = comma]{data/Runge_fxn5_fxn9.csv};
			\addplot[color =black!30!green, no marks, very thick] table [x=x, y=fehler_fxn9, col sep = comma]{data/Runge_fxn5_fxn9.csv};
			
			\legend{ Fehler $n=5$,Fehler $n=9$}
			
			\end{axis}
			\end{tikzpicture}
			\caption{Fehler der approximierten Funktion.}
			\label{fig:runge_lag_fehler}
		\end{subfigure}
		\caption{Lagrange-Approximation der Runge-Funktion mit entsprechendem Fehler, basierend auf \cite[73]{Quarteroni.2006}.}
		\label{fig:runge}
	\end{figure}
	Das Resultat kann man auch mathematisch mithilfe von (\ref{eq:FehLagr}) erklären. Der Grund ist, dass für $n \rightarrow \infty$ die Größenordnung von $ \max_{x \in I}  |f^{(n+1)}(x)|$ die infinitesimale Ordnung von $ \frac{h^{n+1}}{4(n+1)} $ übertrifft.
	
	\subsection{Chebyshev-Interpolation}
	Das Runges-Phänomen kann man bei geeigneter Wahl der Stützstellen verhindern. Werden die Knoten mit 
	\begin{equation}
	x_i =- \frac{b-a}{2} \cos \left( \frac{2i+1}{n+1} \frac{\pi}{2} \right) + \frac{b+a}{2}, \quad i = 0,\dots,n
	\end{equation}
	gewählt, wird das Polynom $ \max_{x \in [a,b]}|w(x)|$ minimal, nämlich
	\begin{equation}
	\max_{x \in [a,b]}|w(x)|= \left(\frac{b-a}{2} \right)^{n+1} 2^{(-n)}.
	\end{equation}
	Daraus folgt, dass das Interpolationspolynom gegen die zu approximierende Funktion für $n$ gegen unendlich konvergiert.
	Die so gewählten Knoten werden auch Chebyshev-Abszissen genannt, vgl. \cite[74]{Quarteroni.2006}.
	In Abbildung \ref{fig:cheby_plot_cheby} sieht man, dass bei Chebyshev-Abszissen die Funktion am Rand nicht ausschlägt, was jedoch bei äquidistanter Wahl der Stützstellen passiert, wie in Abbildung \ref{fig:cheby_plot_pn}. Betrachtet man den zugehörigen Fehler der Approximationen, so wird der Unterschied noch deutlicher, siehe Abbildung \ref{fig:cheby_plot_fehler}. 
	\begin{figure}[h!]
		\centering
		\begin{subfigure}{.45\textwidth}
			\centering
			\begin{tikzpicture}
			\begin{axis}[xlabel=xlabel={$x$}, ylabel = {$f(x)$}, width =\textwidth, height=.8\textwidth, ylabel near ticks, xlabel near ticks, ymin=-0.5, ymax=2,xmin=-1, xmax=1,legend style={at={(0.5,-0.1)},anchor=north}]
			\addplot[blue, no marks, very thick] table [x=x, y=y, col sep = comma]{data/Cheby_plot.csv};
			\addplot[red, no marks, very thick] table [x=x, y=pncheby, col sep = comma]{data/Cheby_plot.csv};
			
			\legend{Runge Funktion, Chebyshev $n=9$}
			
			\end{axis}
			\end{tikzpicture}
			\caption{Runge-Funktion mit Chebyshev-Abszissen interpoliert.}
			\label{fig:cheby_plot_cheby}
		\end{subfigure}
		\begin{subfigure}{.45\textwidth}
			\centering
			\begin{tikzpicture}
			\begin{axis}[xlabel={$x$}, ylabel = {$f(x)$}, width =\textwidth, height=.8\textwidth, ylabel near ticks, xlabel near ticks, ymin=-0.5, ymax=2,xmin=-1, xmax=1,legend style={at={(0.5,-0.1)},anchor=north}]
			\addplot[blue, no marks, very thick] table [x=x, y=y, col sep = comma]{data/Cheby_plot.csv};
			\addplot[color =black!30!green, no marks, very thick] table [x=x, y=pn, col sep = comma]{data/Cheby_plot.csv};
			\legend{Runge Funktion, Lagrange $n=9$}
			\end{axis}
			\end{tikzpicture}
			\caption{Runge-Funktion mit äquidistanten Stützstellen interpoliert.}
			\label{fig:cheby_plot_pn}
		\end{subfigure}
		\caption{Interpolation der Runge-Funktion im Vergleich: Chebyshev \& Lagrange, basierend auf \cite[73]{Quarteroni.2006}. }
		\label{fig:cheby_plot}
	\end{figure}
	
	
	\begin{figure}[h!]
		\centering
		\begin{subfigure}{.45\textwidth}
			\centering
			\begin{tikzpicture}
			\begin{axis}[xlabel={$x$}, ylabel = {$f(x)$}, width =\textwidth, height=.8\textwidth, ylabel near ticks, xlabel near ticks, ymin=-2, ymax=0.5, xmin=-1, xmax=1,legend style={at={(0.5,-0.1)},anchor=north}]
			\addplot[black, no marks, very thick] table [x=x, y=fcheby, col sep = comma]{data/Cheby_plot.csv};
			
			
			\legend{Fehler Chebyshev}
			
			\end{axis}
			\end{tikzpicture}
			\caption{Runge-Funktion mit Chebyshev-Abszissen interpoliert.}
			\label{fig:cheby_plot__fehler_fcheby}
		\end{subfigure}
		\begin{subfigure}{.45\textwidth}
			\centering
			\begin{tikzpicture}
			\begin{axis}[xlabel={$x$}, ylabel = {$f(x)$}, width =\textwidth, height=.8\textwidth, ylabel near ticks, xlabel near ticks,  ymin=-2, ymax=0.5,xmin=-1, xmax=1,legend style={at={(0.5,-0.1)},anchor=north}]
			\addplot[black, no marks, very thick] table [x=x, y=fpn, col sep = comma]{data/Cheby_plot.csv}; %x,y,pncheby,pn,fcheby,fpn
			
			\legend{Fehler Lagrange}
			\end{axis}
			\end{tikzpicture}
			\caption{Runge-Funktion mit äquidistanten Stützstellen interpoliert.}
			\label{fig:cheby_plot_fehler_pn}
		\end{subfigure}
		\caption{Fehler der Interpolation der Runge-Funktion im Vergleich: Chebyshev \& Lagrange, basierend auf \cite[74]{Quarteroni.2006}. }
		\label{fig:cheby_plot_fehler}
	\end{figure}	
	In der Praxis eignet es sich in der Regel eher stückweise in kleinen Intervallen vorzugehen, statt sehr große $n$ zu wählen, siehe Abschnitt \ref*{sec:splines}.
	\newpage
	\subsection{Anwendungen der Polynominterpolation}
	Im Folgenden betrachten wir Anwendungsfälle, bei denen die Polynominterpolation oft gute Ergebnisse liefert.
	\begin{enumerate}
		\item \textbf{Approximation einer Funktion auf einem Intervall}: Auf einem Intervall mit überschaubarer Anzahl an Stützstellen eignet sich das Verfahren gut, vorausgesetzt die Eigenschaften von $f(x)$ ähneln dem von einem Polynom, sonst kann es zur Oszillation kommen. Außerhalb des Intervalls ist die Approximation in der Regel ungenau, da ein Polynom mit $x$ gegen unendlich gegen plus oder minus unendlich strebt.
		\item \textbf{Inverse Interpolation}: Polynominterpolation eignet sich gut, um die inverse Funktion zu approximieren. Man vertauscht die Funktionswerte der Stützstellen mit den Knoten und approximiert die so erhaltenen Stützstellen. 
		\item \textbf{Numerische Integration}: Mit der Polynominterpolation erhält man Funktionen, die analytisch leicht handhabbar sind. Dies wird bei der Numerischen Integration ausgenutzt.
		\item \textbf{Numerische Differentiation}: Wie für die Integration gilt auch für Differentiation, dass Polynome analytisch weniger aufwendig sind als die Ausgangsfunktion.
		
	\end{enumerate}	
	
	
	\subsection{Trigonometrische Interpolation}
	Die Polynominterpolation kann auf verschiedene Typen erweitert werde. Zum Beispiel auf die trigonometrische Interpolation: Statt Polynomen werden trigonometrische Polynome zum Approximieren verwendet, was zur Fourieranalyse führt. Für den trigonometrischen Interpolator $\tilde{f} $ erhält man durch die Linearkombination von Sinus und Cosinus für gerade  $n$ 
	\begin{equation}
	\tilde{f}(x)= \frac{b_0}{2} + \sum_{k=1}^{M} \left[ b_k \cos(kx)+c_k\sin(kx)\right], 
	\label{eq:fourier_ngerade}
	\end{equation}
	wobei $M=n/2$, und für ungerade $n$ 
	\begin{equation}
	\tilde{f}(x) = \frac{b_0}{2} + \sum_{k=1}^{M}\left[b_k\cos(kx)+c_k\sin(kx)\right]+b_{M+1}\cos((M+1)x)
	\label{eq:fourier_nungerade}
	\end{equation}
	mit M = $(n-1)/2$. Man kann die Reihe auch als komplexe Reihe 
	\begin{equation}
	\tilde{f}(x)=\sum_{k=-M}^{M}d_ke^{ikx}
	\label{eq:fourier_c}
	\end{equation}
	darstellen mit der imaginären Einheit $i$ \cite[75]{Quarteroni.2006}.
	Zwischen den Koeffizienten besteht die Beziehung
	\begin{align}
	b_k =& d_k + d_{-k} \\
	c_k =& i(d_k - d_{-k}), \quad k=0,\dots,M. 
	\label{eq:fourier_koeff}
	\end{align}
	Mit der Euler Formel $ e^{ikx} = \cos(kx) + i \sin(kx)$ kann man aus \eqref{eq:fourier_c} zeigen
	\begin{align}
	\sum_{k=-M}^{M}d_ke^{ikx} &= \sum_{k=-M}^{M}d_k(\cos(kx)+i \sin(kx))\\
	&= \sum_{k=-M}^{M}\left[d_k (\cos(kx)+isin(kx))+d_{-k}(\cos(kx)-isin(kx))\right] + d_0,
	\end{align}
	woraus wiederum mit \eqref{eq:fourier_koeff} die Gleichung für gerade $n$ folgt.
	Analog erhalten wir zu \eqref{eq:fourier_nungerade}
	\begin{equation}
	\tilde{f}(x)= \sum_{k=-(M+ \mu )}^{M+ \mu} d_ke^{ikx},
	\end{equation} 
	wobei $\mu = 0$, falls $n$ gerade ist, und $\mu=1$, falls n ungerade ist. Wegen der Analogie zur Fourier-Reihenentwicklung wird $\tilde{f}$ auch \textit{diskrete Fourier-Transformierte} genannt \cite[76]{Quarteroni.2006}.	
	%	    \begin{figure}[h!]
	%	    	\centering
	%	    		\begin{tikzpicture}
	%	    		\begin{axis}[xlabel={X-Achse}, ylabel = {Y-Achse}, width =\textwidth, height=.8\textwidth, ylabel near ticks, xlabel near ticks, ymin=-2, ymax=0.5, xmin=-1, xmax=1,legend style={at={(0.5,-0.1)},anchor=north}]
	%	    		\addplot[black, no marks, very thick] table [x=x, y=fcheby, col sep = comma]{Cheby_plot.csv};
	%	    		
	%	    		
	%	    		\legend{Fehler Chebyschev}
	%	    		
	%	    		\end{axis}
	%	    		\end{tikzpicture}
	%	    		\caption{Runge-Funktion mit Chebyschev-Abszissen interpoliert.}
	%	    		\label{fig:cheby_plot__fehler_fcheby}
	%	    
	%	
	%	    	\caption{Interpolation der Runge-Funktion im Vergleich: Chebyschev \& Lagrange. }
	%	    	\label{fig:cheby_plot_fehler}
	%	    \end{figure}    
	Verwenden wir die Interpolationsbedingungen in den Knoten $x_j = jh$ mit $h=2\pi / (n+1)$ erhalten wir
	\begin{equation}
	\sum_{k=-(M+ \mu )}^{M+ \mu} d_ke^{ikx_j}= f(x_j),\quad j=0,...,n.
	\end{equation}    
	Aus dieser Gleichungen lässt sich ein expliziter Ausdruck für die Koeffizienten von $\tilde{f}$, siehe \eqref{eq:phi_lin}, ableiten 
	\begin{equation}
	d_m = \frac{1}{n+1} \sum_{j=0}^{n}f(x_j)e^{-imjh}, \quad m=-(M+\mu ),...,M+ \mu 
	\label{eq:d_m}
	\end{equation}
	\cite[77]{Quarteroni.2006}.
	In Abbildung \ref{fig:trigo_plot} sieht man das Ergebnis, wenn man zum Beispiel $f(x) = x(x-2\pi) e -x$ mit einem trigonometrischen Interpolator approximiert.
	\begin{figure}[h!]
		\centering
		\begin{tikzpicture}
		\begin{axis}[xlabel={$x$}, ylabel = {$f(x)$}, width =.45\textwidth, height=.45\textwidth, ylabel near ticks, xlabel near ticks, xmin=0, xmax=6,legend style={at={(0.5,-0.15)},anchor=north}]
		\addplot[black, no marks, very thick] table [x=xp, y=yp, col sep = comma]{data/Trigo_plot.csv};
		\addplot[orange, no marks, very thick] table [x=xp, y=z, col sep = comma]{data/Trigo_plot.csv};
		\legend{$f(x)$, Interpolator}
		\end{axis}
		\end{tikzpicture}
		\caption{Die Funktion $f(x) = x(x-2\pi)e -x$ mit einem trigonometrischen Interpolator bezüglich zehn äquidistanter Knoten approximiert \cite[78]{Quarteroni.2006}. }
		\label{fig:trigo_plot}
	\end{figure}
	In manchen Fällen kann die trigonometrische Interpolation sehr ungenau sein, zum Beispiel wenn man die Funktion $f_a(x)= \sin(x) + \sin(5x) $ mit neun äquidistanten Stützstellen auf dem Intervall $[0,2\pi]$ interpoliert. In Abbildung \ref{fig:trigo_plot2} sieht man, das der Interpolator stückweise sogar Phasenverkehrt zu der zu approximierenden Funktion ist. 
	\begin{figure}[h!]
		\centering
		\begin{tikzpicture}
		\begin{axis}[xlabel={$x$}, ylabel = {$f(x)$}, width =.45\textwidth, height=.45\textwidth, ylabel near ticks, xlabel near ticks, xmin=0, xmax=6,legend style={at={(0.5,-0.15)},anchor=north}]
		\addplot[orange, no marks, very thick] table [x=Var1, y=yy, col sep = comma]{data/Trigo_plot2_xx.csv};
		\addplot[black, no marks, very thick] table [x=Var1, y=Var2, col sep = comma]{data/Trigo_plot2_xp.csv};
		\addplot[only marks] table [x=x,
		y=y, col sep = comma]{data/Trigo_plot2_xs.csv};
		%,mark=*,mark options={scale=2, fill=red},text mark as node=true
		
		\legend{Interpolator $\tilde{f}(x)$, $f(x)=\sin(x)+\sin(5x)$}
		\end{axis}
		\end{tikzpicture}
		\caption{Die Effekte des Aliasing: Vergleich zwischen der Funktion $f(x) =
			sin(x) + sin(5x)$  und ihrem trigonometrischen Interpolator mit $M = 3$ \cite[79]{Quarteroni.2006}. }
		\label{fig:trigo_plot2}
	\end{figure}
	Der Fehler entsteht, da sich $f_a(x) $ in den betrachteten Knoten nicht von der Funktion $F(x)=sin(x)+sin(3x)$ unterscheidet. Wir approximieren also tatsächlich $F(x)$. Diese Phänomen wird \textit{Aliasing} genannt und tritt immer dann auf, wenn in derselben Funktion Komponenten unterschiedlicher Frequenz vorkommen. Solange die Anzahl der Stützstellen nicht groß genug ist, um die höchsten Frequenzen aufzulösen, können Interferenzen mit niedrigeren Frequenzen auftreten.
	Im Alltag begegnet uns \textit{Aliasing} beim Betrachten eines sich drehenden Speichenrades. Dreht sich das Rad zu schnell, so ist unser Gehirn nicht mehr in der Lage die Situation richtig zu verarbeiten und erzeugt verkehrte Bilder im Kopf \cite[S.78]{Quarteroni.2006}.
	\newpage
	\subsection{Beispiel: Geschwindigkeitskurve}
	Anhand eines Beispieles demonstrieren wir nun die numerische Polynominterpolation einer Kurve mit MatLab.
	In einem Versucht misst ein Geschwindigkeitssensor alle zehn Sekunden die Geschwindigkeit. Anschließend soll aus den erhobenen Werten
	\begin{table}[h!]
		\centering
		\begin{tabular}{|c|c|c|c|c|c|c|c|}
			\hline
			Zeit $[s]$ & 0 & 10 & 20 & 30 & 40 & 50 & 60  \\
			\hline
			Geschwindigkeit $[km/h]$ & 00 & 100 & 50 & 10 & -50 & 20  & 100\\
			\hline
			
		\end{tabular}
		\caption{Erhobene Werte für Zeit und Geschwindigkeit des Versuchs.}
		\label{tab:Werte}
	\end{table}	
	die Geschwindigkeitskurve berechnet werden.\\
	Um den Interpolator zu berechnen, implementieren wir eine geeignete Funktion, welche wir \verb+Lpoly+ nennen. Diese Funktion \lstinputlisting[style=Matlab-editor,caption =  \texttt{Lpoly}-Funktion: Funktionskopf , firstline=1,lastline=1]{data/Lpoly.m}
	erhält vier Parameter: Werte auf der Abszisse, an denen der Interpolator ausgewertet werden soll $x$, Knotenpunkte $xsamples$, Funktionswerte der Knotenpunkte $fsamples$, Grad des Interpolators $N$.\\ 
	Zuerst initialisieren wir den Rückgabe Wert 
	\lstinputlisting[style=Matlab-editor,caption =  \texttt{Lpoly}-Funktion: Initilisierung des Rückgabewertes \texttt{fxn}, firstline=6,lastline=6]{data/Lpoly.m}
	so, dass er die selbe Dimension wie der Parameter $x$  besitzt. Als nächstes überprüfen wir die Stützstellenanzahl,
	\lstinputlisting[style=Matlab-editor,caption =\texttt{Lpoly}-Funktion: Überprüfung der Stützstellenanzahl , firstline=9,lastline=13]{data/Lpoly.m}
	ob sie ausreicht, ein Polynom vom Grad $N$ zu approximieren. Ist dies nicht der Fall, wird eine Fehlermeldung ausgegeben. Wird der Algorithmus nicht abgebrochen, wird das Lagrange-Polynom \eqref{eq:Lagrange_formel} mithilfe von 
	\lstinputlisting[style= Matlab-editor, caption =  \texttt{Lpoly}-Funktion: Berechnung des Lagrangepolynoms ,firstline=15,lastline=27]{data/Lpoly.m}
	berechnet. Die Äußere \verb+for+-Schleife mit dem Index \verb+idx+ stellt die Summe 
	\begin{equation}
	\sum_{k=0}^{n}y_k L_k(x), \quad k=0,...,n
	\end{equation}
	dar. Die entsprechenden $L_k(x)$ 
	\begin{equation}
	L_k(x) =	\prod_{\substack{i=0 \\ i \neq k }}^{n} \frac{x-x_i}{x_k-x_i}, k=0,...,n
	\end{equation}
	werden in der inneren \verb+for+-Schleife berechnet. Der Rückgabe Wert ist damit ein Vektor, welcher die Funktionswerte des Interpolationspolynoms an entsprechenden x Werten enthält. Hierbei wurde der Interpolator mit den vorgegebenen Stützstellen erstellt.\\
	Wendet man diese Funktion auf die erhobenen Werte an, erhält man den Graphen in Abbildung \ref{fig:Gesch_plot}.
	\begin{figure}[h!]
		\centering
		
		\begin{tikzpicture}
		\begin{axis}[xlabel={Zeit in s}, ylabel = {Geschwindigkeit in km/h}, width =.75\textwidth, height=.4\textwidth, ylabel near ticks, xlabel near ticks, xmin=0, xmax=60]
		\addplot[blue, no marks, very thick] table [x=x, y=fxn, col sep = comma]{data/Geschwindigkeit_x_fxn.csv};
		\addplot[only marks,mark=*,mark options={scale=2, fill=red},text mark as node=true] table [x=t,
		y=a, col sep = comma]{data/Geschwindigkeit.csv};
		%text mark as node=true] table [x=xsamples_9, y=fsamples_9, col sep = comma]{Runge_pt9.csv};
		
		\legend{Interpolator, Daten}
		\end{axis}
		\end{tikzpicture}
		\caption{Daten aus der Tabelle \ref{tab:Werte} interpoliert mit einem Polynom $6.$ Grades.}
		\label{fig:Gesch_plot}	
	\end{figure}\\	
	Die \texttt{Lpoly}-Funktion ist nur eine Möglichkeit das Verfahren zu implementieren. Eine weitere Implementierung wäre zum Beispiel, wenn man das Problem auf zwei Funktionen aufteilt. So könnten zum Beispiel die Koeffizienten des Interpolators in der einen Funktion berechnet werden, 
	und die Funktionswerte in einer Anderen. Diese Variante hat den Vorteil, dass ein bereits berechneter Interpolator zwischen gespeichert wird. Bei vermehrter Verwendung muss er so nicht mehrmals neu berechnet werden. Außerdem kann das Polynom so auch einzeln betrachtet werde. 
	Mit den Befehlen \verb+polyfit+ und \verb+polyval+ ist diese Variante auch schon in Matlab implementiert \cite[70]{Quarteroni.2006}. Wobei der erste Befehl die Koeffizienten des Interpolators berechnet, und der zweite den Interpolator, gegeben über seine Koeffizienten, an entsprechenden Stellen auswertet.

	
	
	
	
