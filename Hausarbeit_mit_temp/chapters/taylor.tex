

\chapter{Hauptteil}
		\section{Taylor-Approximation}\label{sec:Taylorapp}
		Handelt es sich um eine bekannte Funktion, die jedoch sehr schwierig differenzierbar bzw. integrierbar ist, beispielsweise bei der
		mathematischen Beschreibung physikalischer Effekte, so ist die Taylor-Approximation ein
		häufig genutztes und oft einfach anwendbares Verfahren. Die Taylor-Approximation nutzt Ableitungsinformationen
		der Ursprungsfunktion an einem Punkt, um sie durch ein Polynom mit gleichem Verhalten in Steigung und Krümmung 
		in der Umgebung dieses Punktes anzunähern. Polynome haben die Eigenschaft, besonders einfach integrierbar und 
		differenzierbar zu sein und eigenen sich daher sehr gut dazu, Rechenaufwand einzusparen. Die allgemeine Formel für 
		ein Taylorpolynom $T_{n}(x)$ der Ordnung $n$ mit der $k$-ten Ableitung $f^{(k)}$ um den Entwicklungspunkt $x_0$ ist gegeben durch\\
		\begin{align}
		T_{n}(x)=\sum_{k=0}^{n}\frac{f^{(k)}(x_0)}{k!}(x-x_0)^{k}+R_{n}(x)\text{.}
		\end{align}
		Hierbei steht $R_{n}$ für das sogenannte Restglied, welches eine Abschätzung des bei der Approximation
		gemachten Fehlers darstellt \cite[191 \psq]{Strampp.2015}. Für diese Abschätzung gibt es diverse Verfahren, auf welche wir hier nicht weiter
		eingehen werden. Das eigentliche Polynom und dessen Formel können sehr gut am Beispiel der Kosinus-Funktion und deren 
		Annäherung um den Entwicklungspunkt $x_0=0$ erläutert werden. Ohne Restglied ergibt sich beispielsweise die allgemeine
		Darstellung des Taylor-Polynoms um den Punkt $x_0$ vom Grad 4 zu
		\begin{align}
		T_{4}(x)=f(x_{0})+\frac{f^{\prime}(x_{0})}{1!}x+\frac{f^{\prime \prime}(x_{0})}{2!}x^{2}+\frac{f^{\prime \prime \prime}(x_{0})}{3!}x^{3}+\frac{f^{\prime\prime \prime \prime}(x_{0})}{4!}x^{4}.
		\end{align}
		Der erste Summand stellt sicher, dass das Polynom im Entwicklungspunkt den selben Funktionswert aufweist, wie die 
		anzunähernde Funktion selbst. Da alle weiteren Summanden mindestens einmal mit $(x-x_0)$ multipliziert werden, verschwinden 
		diese am Entwicklungspunkt $x=x_0$. Am Beispiel des Kosinus würde sich der erste Summand also zu $1$ ergeben
		\begin{align}
		T_{4}(x)=1+\frac{f^{\prime}(x_{0})}{1!}x+\frac{f^{\prime \prime}(x_{0})}{2!}x^{2}+\frac{f^{\prime \prime \prime}(x_{0})}{3!}x^{3}+\frac{f^{\prime\prime \prime \prime}(x_{0})}{4!}x^{4}.
		\end{align} 
		Nun soll die Funktion nicht nur ihren Funktionswert am Entwicklungspunkt mit der zu approximierenden Funktion teilen, sondern sich auch 
		möglichst nahe an diese anschmiegen, um ein Intervall um den Entwicklungspunkt zu erhalten, in dem das Polynom möglichst ähnliche Werte liefert.
		Um dies zu erreichen, müssen die Ableitungen der Ursprungsfunktion betrachtet werden, da diese Informationen, wie Steigung oder Krümmung des Graphen, codieren.
		Die erste Ableitung des Kosinus ist der Sinus, welcher sich an der Entwicklungsstelle zu $0$ ergibt. Der zweite Summand ist somit $0$.
		Die zweite Ableitung liefert $(\cos(0))^{\prime \prime}=-\cos(0)=-1$. Damit erhält man einen weiteren Summanden und dadurch
		\begin{align}
		T_{4}(x)=1+0-\frac{1}{2!}x^{2}+\frac{f^{\prime \prime \prime}(x_{0})}{3!}x^{3}+\frac{f^{\prime\prime \prime \prime}(x_{0})}{4!}x^{4}\text{.}
		\end{align}
		Analoges Vorgehen für die dritte und vierte Ableitung ergibt schließlich die gesuchte Taylor-Approximation vierten Grades der Kosinus-Funktion
		\begin{align}
		T_{4}(x)=1+0-\frac{1}{2!}x^{2}+0+\frac{1}{4!}x^{4}\text{.}
		\end{align}   
		Leitet man dieses Polynom zweimal ab, verschwinden alle konstanten und linearen Teilpolynome. Der quadratische Term wird gerade zu $-1$, was genau dem Wert der Ableitung
		im Entwicklungspunkt entspricht. Alle Terme höherer Ordnung verschwinden wieder, da sie ein $x$ enthalten. Hieraus lässt sich gut eine intuitive Vorstellung für das Auftreten 
		der Fakultäten im Nenner ableiten. Da es sich um Polynome handelt, muss bei jedem Ableiten die Potenzregel angewandt werden, was den Vorfaktor verändert. Dieser soll aber nach 
		$n$-maligem Differenzieren gerade dem Wert der $n$-ten Ableitung entsprechen. Die Fakultäten im Nenner kompensieren also die Vorfaktoren, die durch das wiederholte Anwenden der Potenzregel entstehen.\\
		\begin{figure}[h!]
			\centering
			\begin{tikzpicture}
			\begin{axis}[
			clip=true,
			xlabel=$x$,
			ylabel={$f(x)$},
			xmin=-2*pi,xmax=2*pi,
			ymin=-3,ymax=3,
			grid=major,
			grid style=dashed,
			xtick={-6.28,-4.71,-3.14,-1.57,0,1.57,3.14,4.71,6.28},
			xticklabels={$-2\pi$,$-\frac{3}{2}\pi$,$-\pi$,$-\frac{\pi}{2}$,$0$, $\frac{\pi}{2}$,$\pi$,$\frac{3}{2}\pi$,$2\pi$},
			ytick={-2,-1,0,1,2},
			yticklabels={$-2$,$-1$,$0$,$1$,$2$}
			]
			\addplot[domain=-2*pi:2*pi,domain y=-3:3,samples=200,black]{cos(deg(x))}node[right,pos=0.9]{};
			\addplot[domain=-2*pi:2*pi,domain y=-3:3,samples=200,red]{1-0.5*x*x}node[right,pos=0.9]{};
			\addplot[domain=-2*pi:2*pi,domain y=-3:3,samples=200,blue]{1-0.5*x*x+1/24*x*x*x*x}node[right,pos=0.9]{};
			\legend{$\cos(x)$,$T_{2}(x)$,$T_{4}(x)$}
			\end{axis}		
			\end{tikzpicture}
			\caption{Taylor-Approximation der Kosinus-Funktion um $x_0=0$.}
			\label{fig:TaylorKosinus} 
		\end{figure}\\
		In Abbildung \ref{fig:TaylorKosinus} lässt sich das anfänglich beschriebene Anschmiegen des Polynoms mit zunehmender Ordnung gut erkennen.
		Für mehr und mehr zusätzliche Summanden erhält man eine immer bessere Approximation der vorgegebenen
		Funktion. Setzt man dies theoretisch immer weiter fort erhält man eine unendliche Summe, eine sogenannte Taylor-Reihe.
		Taylor-Reihen-Entwicklungen und Taylor-Approximationen sind sehr eng Verwandte Konzepte. Bei der Taylor-Reihe wird meist die Konvergenz untersucht.
		Kann man eine Funktion in jedem Punkt durch ihre Taylor-Reihe darstellen, so spricht man von einer analytischen Funktion \cite[102]{Kerner.2013}. 
		Analoges lässt sich auch nur für bestimmte Intervalle definieren. 
		Alles Erwähnte lässt sich zudem  auch für Funktionen mehrerer Variablen anwenden und führt zur mehrdimensionalen Taylor-Approximation.\\
		\begin{figure}[h]
			\centering
			\begin{tikzpicture}
			\begin{axis}[
			xlabel=$x$,
			ylabel=$y$,
			zlabel={$f(x,y)$},
			xmin=-2,xmax=2,
			ymin=-2,ymax=2,
			zmin=-2,zmax=2]
			\addplot3[surf,fill=white,domain=-2:2] {-2*exp(-x^2-y^2)};
			\addplot3[surf,fill=blue,domain=-1:1] {-2*(1-x^2-y^2)};
			\legend{$-2e^{-x^{2}-y^{2}}$, $T_{4}(x \text{,} y)$}	
			\end{axis}		
			\end{tikzpicture}
			\caption{Mehrdimensionale Taylor-Approximation um Koordinatenursprung.}
			\label{Taylor3D}
		\end{figure}\\
		In drei Dimensionen kann diese, wie in Abbildung \ref{Taylor3D}, beispielsweise als eine sich einem Höhenprofil anschmiegende Ebene veranschaulicht werden.\\
		Wir haben gesehen, dass sich die Taylor-Approximation eignet, um einfache Polynome zu erhalten, die selbst komplizierte Funktionen in der Nähe des Entwicklungspunktes
		effizient annähern können. Tatsächlich wird dies in der Physik und den Ingenieurwissenschaften sehr oft genutzt, um beispielsweise 
		trigonometrische Funktionen oder die e-Funktion zu approximieren. Die gewählte Ordnung hängt hierbei von der Problemstellung und der maximalen akzeptablen Abweichung 
		ab.\\
		Bei all dem ist sie jedoch nur unter gewissen Voraussetzungen anwendbar. Handelt es sich beispielsweise um eine nicht analytische Funktion, so lässt sich diese nicht in eine Taylorreihe entwickeln.
		Man betrachte die Funktion\\
		\begin{align}
		f(x)=
		\begin{cases}
		e^{-\frac{1}{x^2}}& ,x\neq0\\
		0& ,x=0
		\text{.}
		\end{cases}
		\end{align}
		Versucht man nun ein Taylorpolynom um den Punkt $x_0=0$ zu entwickeln, so betrachtet man zunächst die Ableitungen. Für $x\neq 0$ erhält man \\
		\begin{align}
		f^{\prime}(x)=\frac{2}{x^3}e^{-\frac{1}{x^2}}\text{.}
		\end{align}
		Diese konvergiert für $x\rightarrow0$ gegen $0$. Die Ableitung im Punkt $x=0$ ergibt sich zu 
		\begin{align}
		f^{\prime}(0)=\lim_{\epsilon\longrightarrow 0}\frac{f(\epsilon)-f(0)}{\epsilon}=0\text{.}
		\end{align}
		Die Ableitung existiert also und ist stetig. Höhere Ableitungen können analog berechnet werden. Jedoch ergeben sich alle Ableitungen im Punkt $x=0$ zu $0$. Die Taylorreihe lässt sich also bilden, aber stimmt 
		nicht mit der Funktion $f(x)$ überein. Das Taylorpolynom ergibt sich damit für beliebige Ordnung $n$ zu $0$.\\
		Davon abgesehen existieren Funktionen, deren Taylorentwicklung nur innerhalb eines Intervalls gegen die Funktion divergieren. Man spricht von dem oben erwähnten 
	    Konvergenzintervall, vgl. \cite[Kapitel 37]{Estep.2005}. Ein Beispiel hierfür ist die Funktion des natürlichen Logarithmus um den Entwicklungspunkt $x_0=1$.
	    \newpage
		\begin{figure}[h]		
			\centering
			\begin{tikzpicture}
			\begin{axis}[
			clip=true,
			xlabel=$x$,
			ylabel={$\ln(x)$},
			xmin=-1,xmax=6,
			ymin=-2,ymax=2,
			grid=major,
			grid style=dashed,
			xtick={-1,0,1,2,3,4,5,6},
			xticklabels={$-1$,$0$,$1$,$2$,$3$,$4$,$5$,$6$},
			ytick={-2,-1,0,1,2},
			yticklabels={$-2$,$-1$,$0$,$1$,$2$}
			]
			\addplot[domain=-1:6,domain y=-2:2,samples=200,black]{ln x}node[right,pos=0.9]{};
			\addplot[domain=-1:6,domain y=-2:2,samples=200,blue]{x-1-(x-1)*(x-1)/2}node[right,pos=0.9]{};
			\addplot[domain=-1:6,domain y=-2:2,samples=200,color =black!30!green]{x-1-(x-1)*(x-1)/2+(x-1)*(x-1)*(x-1)*(x-1)/4}node[right,pos=0.9]{};
			\legend{$\ln(x)$,$T_{2}(x)$,$T_{4}(x)$}
			\end{axis}							
			\end{tikzpicture}
			\caption{Taylor-Approximation des natürlichen Logarithmus.}
			\label{fig:ln-plot} 			
		\end{figure}
		Innerhalb des Intervalls $[0,2]$ lässt sich das Verfahren wie oben beschrieben anwenden. Außerhalb des Intervalls versagt die Approximation jedoch, 
		da die entsprechende Taylor-Reihen-Entwicklung divergiert und somit die erhaltenen Funktionswerte bei Auswertung der Taylor-Approximation außerhalb des Intervalls
		mit zunehmender Ordnung mehr und mehr von der anzunähernden Funktion abweichen, siehe Abbildung \ref{fig:ln-plot}.
		Abgesehen von diesen Einschränkungen muss bereits eine (zumindest im Umfeld des Entwicklungspunktes) stetig differenzierbare Funktion analytisch berechnet worden sein.
		Die Nähe des Entwicklungspunktes zu Unstetigkeiten kann für erhebliche Abweichungen außerhalb gewisser Intervallgrenzen führen \cite[598]{Estep.2005}. 
		In der Realität ist dies jedoch gerade in praktischen Anwendungen oftmals nicht der Fall.\\
		Will man Funktionen in großen Bereichen auswerten oder aus gemessenen Daten eine Funktion konstruieren, die den Sachverhalt möglichst gut darstellt, muss man also zu anderen Verfahren greifen.