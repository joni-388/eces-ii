\section{Methode der kleinsten Fehlerquadrate}\label{sec:kleinsteQuadrate}
\subsection{Allgemeines}
Mit der Methode der kleinsten Fehlerquadrate kann man zu einer gegebenen Datenmenge $M$ eine Funktion $f(x)$ bestimmen, die diese Daten annähert. \\
Die Art der Funktion ist hierbei abhängig von der Menge $M$. Sofern möglich, besonders bei zweidimensionalen Datenmengen, sollte man sich die Datenmenge graphisch darstellen lassen, um entscheiden zu können, welche Art von Funktion für die Annäherung geeignet ist. Bei dreidimensionalen Problemstellungen ist dies jedoch oft nur bedingt möglich. \\
\\
Im Gegensatz zur Interpolation geht es bei der Methode der kleinsten Fehlerquadrate darum, eine Funktion zu bestimmen, die den Fehler der Datenpunkte zur Funktion minimiert und nicht eine Funktion, die durch jeden dieser Werte verläuft. Teilweise lassen sich so Rückschlüsse auf die folgenden Datenwerte ziehen, oder Zusammenhänge zwischen diesen entdecken. Die Methode "lernt" \space  mit jedem Datenwert dazu, weshalb sie genauer wird, umso mehr Messwerte zur Verfügung stehen. Interpolation und die Methode der kleinsten Fehlerquadrate können häufig für die gleichen Problemstellungen verwendet werden. \\
Anwendung findet sie daher in der Statistik und bei der Sammlung physikalischer Messwerte. Es ist davon auszugehen, dass sich bei solchen Erhebungen stets Messfehler oder Abweichungen ergeben. In der Statistik ist sie häufig unter dem Namen Regression wiederzufinden. Im Folgenden werden wir deshalb auch den Begriff Regression als Synonym für die Methode der kleinsten Fehlerquadrate benutzen.
\\
\\
Die Annahme ist, dass es stets eine Funktion gibt, die die Messwerte annähert. Voraussetzung dafür ist, dass es mindestens einen Messwert gibt, über den die Koeffizienten der Funktion berechnet werden können. Des Weiteren geht man davon aus, dass die erhobenen Daten nur gering von den idealen Werten abweichen, da große Messfehler zu einer starken Veränderung der Koeffizienten führen. Zum Bestimmen des kleinsten Fehlers $f^{2}_{i}$ werden nicht die Abstände der Datenwerte zur Funktion betrachtet, sondern deren Abstand zur Funktion ins Quadrat genommen. Dies birgt den Vorteil, dass sich Fehler nicht gegenseitig aufheben und die Funktion differenzierbar bleibt. Es ist zu beachten, dass zur Bestimmung des Fehlers nicht der senkrechte Abstand , sondern der vertikale Abstand zur Funktion verwendet wird, vgl. \cite[251 \psq]{NguyenSchaefer.2017} und  \cite[ 1 \psq]{Fahrmeir.2009}.
\\ 
\subsection{Lineare Funktionen} 
Können die $n$ Messwerte $y_{i}$ mit Messpunkt $x_{i}$ durch eine lineare Funktion approximiert werden, also eine Funktion der Form $f(x) = mx + b$, so lässt dich der quadratische Fehler in jedem 
Punkt $x_{i}$ mit 
\begin{equation}
\large	f_{i}^{2}(m,b) = (y_{i} - (mx_{i} + b))^{2}
\label{eq:quadrFehler}
\end{equation}
und die Summe des quadratischen Fehlers mit
\begin{equation}
\large F^{2}(m,b) = \sum_{i}^{n} (y_{i} - (mx_{i} + b))^{2}
\label{eq:sumQuadrFehler}
\end{equation}
berechnen.
\\
Das Ziel ist, den Wert der Koeffizienten $m$ und $b$ so zu berechnen, dass die Summe des quadratischen Fehlers minimal wird. Hierzu wird sowohl nach $m$ als auch nach $b$ differenziert. Die Ableitung wird gleich 0 gesetzt
\begin{equation}
\frac{\delta F^{2}(m,b)}{\delta m} = -2\sum_{i}^{n}(y_{i}-(mx_{i}+b)) = 0
\end{equation}
\begin{equation}
\frac{\delta F^{2}(m,b)}{\delta b} = -2\sum_{i}^{n}(y_{i}-(mx_{i}+b))\cdot x_{i} = 0.
\end{equation}
Formt man diese beiden Gleichungen nun um, so ergibt sich ein lineares Gleichungssystem der Form
\begin{equation}
\begin{bmatrix}
n & \sum_{i}^{n}x_{i} \\
\sum_{i}^{n}x_i & \sum_{i}^{n}x_{i}^{2}
\end{bmatrix}
\begin{pmatrix}
b\\
m
\end{pmatrix}
= 
\begin{pmatrix}
\sum_{i}^{n}y_{i}\\
\sum_{i}^{n}x_{i}y_{i}
\end{pmatrix}.
\end{equation}
Das Gleichungssystem ist nach den Parametern $m$ und $b$ aufzulösen. Somit erhält man nach \cite[ 251 \psqq]{NguyenSchaefer.2017} für die Steigung $m$ mit Hilfe der arithmetischen Mittel $\bar{x} = \frac{1}{n} \sum_{i}^{n} x_{i}$ und $\bar{y} = \frac{1}{n}\sum_{i}^{n}y_{i} $
\begin{equation}
m = %\frac{\sum_{i}^{n}x_{i}y_{i}-\frac{1}{n}\sum_{i}^{n}x_{i} \cdot \sum_{i}^{n}y_{i}}{\sum_{i}^{n}x_{i}^{2}-\frac{1}{n}(\sum_{i}^{n}x_{i})^{2}} 
\frac{\sum_{i}^{n}(x_{i}-\bar{x})\cdot(y_{i}-\bar{y})}{\sum_{i}^{n}(x_{i}-\bar{x})^{2}}
\end{equation}
und für den y-Achsenabschnitt $b$
\begin{equation}
b = \bar{y} - m \cdot \bar{x} = \frac{1}{n} \left(\sum_{i}^{n}y_{i}-m\sum_{i}^{n}x_{i}\right). %=  \frac{n\sum_{i}^{n}x_{i}y_{i}-\sum_{i}^{n}y_{i}\sum_{i}^{n}x_{i}}{n\sum_{i}x_{i}^{2}-(\sum_{i}^{n}x_{i})^{2}}  
\end{equation}
\\
\subsection{Polynom Funktionen}
Teilweise lassen sich erhobene Datenmengen mit $n$ Messpunkten $x_{i}$ und $n$ Messwerten $y_{i}$ nicht gut durch eine lineare Funktion annähern. In diesem Fall kann es sinnvoll sein, die Datenpunkte mit Hilfe einer Polynomfunktion vom Grad $m$ anzunähern, also einer Funktion der Form $f(x) = a_{0} + a_{1}x + a_{2}x^{2} + ... + a_{m}x^{m} = \sum_{k=0}^{m}a_{k}x^{k}$. \\
\\
Der quadratische Fehler in einem Messpunkt kann durch 
\begin{equation}
f_{i}^{2}(\textbf{a}) = \left(y_{i}-\sum^{m}_{k=0}a_{k}x_{i}^{k}\right)^{2}
\end{equation}
bestimmt werden. Der gesamte Fehler ergibt sich somit aus
\begin{equation}
F^{2}(\textbf{a}) = \sum_{i}^{n}\left(y_{i}-\sum_{k=0}^{m}a_{k}x^{k}\right)^{2}.
\end{equation}
Die Koeffizienten $a_{k}$ mit $k\in[0...m]$ werden zu dem Vektor $\textbf{a}=(a_{0},a_{1},a_{2},...,a_{k})^{T}$ zusammengefasst.
Zur Berechnung des Minimums wird nach jedem der $m + 1$ Koeffizienten abgeleitet. Durch Nullsetzen der Ableitungen ergibt sich ein lineares Gleichungssystem zur Bestimmung der einzelnen Koeffizienten. Dieses kann mit Hilfe des Gauß-Verfahrens gelöst werden. Nach \cite[8 \psqq]{Froehling} lautet die allgemeine Form des linearen Gleichungssystem
\begin{equation}
\begin{bmatrix}
n & \sum_{i}^{n}x_{i} & \sum_{i}^{n}x_{i}^{} & \dots & \sum_{i}^{n}x_{i}^{m} \\
\sum_{i}^{n}x_{i} & \sum_{i}^{n}x_{i}^{2} & \sum_{i}^{n}x_{i}^{3} & \dots & \sum_{i}^{n}x_{i}^{m+1} \\
\sum_{i}^{n}x_{i}^{2} & \sum_{i}^{n}x_{i}^{3} & \sum_{i}^{n}x_{i}^{4} & \dots & \sum_{i}^{n}x_{i}^{m+2} \\
\vdots & \vdots & \vdots &  & \vdots \\
\sum_{i}^{n}x_{i}^{m} & \sum_{i}^{n}x_{i}^{m+1} & \sum_{i}^{n}x_{i}^{m+2} & \dots & \sum_{i}^{n}x_{i}^{2m} \\
\end{bmatrix}%
\begin{pmatrix}
a_{0} \\
a_{1} \\
a_{2} \\
\vdots \\
a_{m}
\end{pmatrix}
=
\begin{pmatrix}
\sum_{i}^{n}y_{i}\\
\sum_{i}^{n}x_{i}y_{i}\\
\sum_{i}^{n}x_{i}^{2}y_{i}\\
\vdots\\
\sum_{i}^{n}x_{i}^{m}y_{i}\\
\end{pmatrix}.
\label{eq:matrixmult}
\end{equation}
\\
\\
\subsection{Beispiel: Ohmsche Gesetz}
Die Funktionsweise und Genauigkeit der linearen Regression werden wir nun Anhand eines Beispiels in Matlab implementieren. Gegeben sei das bekannte Ohmsche Gesetz $$U = R \cdot I$$ aus der Elektrotechnik. $U$ steht für die Spannung in Volt, $R$ für einen Widerstand in Ohm, in diesem Beispiel beträgt $R$ den Wert \SI{250}{\ohm} und $I$ sei eine Variable Stromquelle mit Einheit Ampere.
Bei einer Messung der Spannung am Verbraucher wurden 50 Messwerte erhoben, der anfängliche Strom, der fließt beträgt \SI{0,01}{\ampere} und wurde bei jedem Messwert um \SI{0,01}{\ampere} erhöht. Durch
Fehler bei der Messung weicht die gemessene Spannung jedoch um $\pm$ \SI{1}{\volt} von dem, mit dem Ohmschen Gesetz berechneten, Idealwert der Spannung ab.\\
\begin{figure}[h!]
	%\centering{.5\textwidth}
	\centering
	\includegraphics[width=0.5\textwidth]{data/messwerte}
	\caption{Bei der Messung erhobene Spannungswerte}
	\label{fig:messwerte}
\end{figure}
Werden nun die gegeben Messwerte aus Abbildung \ref{fig:messwerte} betrachtet, so wird eindeutig, dass es sich für dieses Beispiel anbietet eine lineare Regressionsgerade zu erstellen, um die Messwerte anzunähern.
Gesucht ist also eine mathematische Funktion der Form$f(x) = mx + b$.\\
\\
Die in Matlab implementierte Funktion \verb+myLinearPolyfit(x,y)+ erhält zwei Vektoren \textbf{x} und \textbf{y} mit gleicher Länge. Im Vektor \textbf{x} sind die Messpunkte, im Vektor \textbf{y} die dazugehörigen Messwerte gespeichert.
%
\lstinputlisting[style=Matlab-editor, caption={Aufruf der \texttt{myLinearPolyfit}-Funktion}, ]{data/myLinearPolyfit.m}
%
Die Messpunkte und Messwerte werden zur Berechnung der Steigung $m$ und des y-Achsenabschnitts $b$ gebraucht. Die Funktion errechnet die Steigung, indem sie die separate Funktion \verb+calculateM+ aufruft und den y-Achsenabschnitt mit der Funktion \verb+calculateB+, welche im Anhang zu finden sind. Die Funktion \verb+myLinearPolyfit+ liefert einen zweidimensionalen Vektor zurück, dessen erster Wert die Steigung und zweiter Wert der y-Achsenabschnitt ist.\\ 
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{data/ausgleichsgerade}
	\caption{Die berechnete Regressionsgerade für die erhobenen Spannungswerte.}
	\label{fig:gerade}
\end{figure}\\
In Abbildung \ref{fig:gerade} sieht man die von \texttt{polyfit} berechnete Gerade. Die Funktion lautet $f(x) = 2,5037x - 0,3521$, vergleicht man diese nun mit der Ausgangsfunktion $f_{a}(x) = 2,5x$, um welche die Messwerte zufällig mit $\pm$ \SI{1}{\volt} verteilt wurden, so zeigt sich, dass die Steigung von $f(x)$ nahezu identisch mit der von $f_{a}(x)$ ist. Hier tritt jedoch ein Problem zwischen theoretischer Annäherung und der Realität auf, der y-Achsenabschnitt befindet sich bei $b = \SI{-0,3521}{\volt}$. Berechnet man die Spannung mit dem Ohmschen Gesetz bei \SI{0}{\ampere}, so ist auch die Spannung gleich \SI{0}{\volt}. Dieses Problem entsteht dadurch, dass die Regressionsgerade auch bei einem Strom von \SI{0}{\ampere} von einem Messfehler ausgeht.\\
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{data/fehlerquadrate}
	\caption{Graphische Darstellung der entstehenden Fehlerquadrate zwischen gemessener Spannung und der Regressionsgerade.}
	\label{fig:fehlerquadrate}
\end{figure}\\
\newpage
Abbildung \ref{fig:fehlerquadrate} zeigt beispielhaft, wie die Fehlerquadrate berechnet werden.
Der Messwert $y_{3}$ im Messpunkt $x_{3} = \SI{0,03}{\ampere}$ beträgt \SI{6,593}{\volt}, wendet man nun Formel \eqref{eq:quadrFehler} an, so beträgt im Punkt $x_{3}$ der quadratischen Fehler  $f_{3}^{2} \approx (\SI{-0,566}{\volt})^{2} \approx \SI{0,3204}{\square\volt}$. In diesem Ausschnitt wird auch ersichtlich, wieso der Fehler ins Quadrat genommen wird. Würde man die Fehler nicht quadrieren, so höben sie sich auf, da manche Fehler negativ und andere positiv sind.\\
Der gesamte quadratische Fehler ergibt sich mit \eqref{eq:sumQuadrFehler} zu \SI{18,672}{\square\volt}. Und damit ein durchschnittlicher Fehler von \SI{0,3734}{\square\volt}. Zur Bestimmung der Güte bzw. Bestimmtheit der Regression wird die Formel 
\begin{equation}
R^{2} = \frac{\sum_{i}^{n}(\hat{y}_{i}-\bar{y}_{i})^{2}}{\sum_{i}^{n}({y}_{i}-\bar{y}_{i})^{2}} = \frac{\sum_{i}^{n}((m \cdot x_{i} + b)-\bar{y}_{i})^{2}}{\sum_{i}^{n}({y}_{i}-\bar{y}_{i})^{2}}
\label{eq:guete}
\end{equation}
genutzt, siehe \cite[114]{Fahrmeir.2009}. Hierbei stellt $\bar{y}_{i}$ das arithmetische Mittel der Messwerte dar und wird mit $\bar{y}_{i} = \frac{1}{n}\sum_{i}^{n}y_{i}$ berechnet.\\
\\
Mit Hilfe der Formel \eqref{eq:guete} lässt sich eine Güte berechnen. Diese ist dimensionslos und beträgt $0,9997$. Unsere Annäherung eignet sich somit sehr gut dazu, die Messwerte abzubilden.
\begin{figure}[h!]
	\centering
	\begin{subfigure}{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{data/wenigeMesswerte}
		\caption{Lineare Regression mit wenigen Messwerten und einem großen Fehler.}
		\label{fig:wenigeMesswerte}
	\end{subfigure}%
	\begin{subfigure}{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{data/vieleMesswerte}
		\caption{Lineare Regression mit vielen Messwerten und einem großen Fehler.}
		\label{fig:vieleMesswerte}
	\end{subfigure}
	\caption{Vergleich der Auswirkung von großen Messfehlern auf die Annäherung.}
	\label{fig:Vergleich}
\end{figure}
In den Abbildungen \ref{fig:wenigeMesswerte} und \ref{fig:vieleMesswerte} wird ersichtlich, was mit den eingangs beschriebenen Aussagen, dass die Funktion genauer wird je mehr Messpunkte zur Verfügung stehen und wie sehr sich große Abweichungen in den Messungen auf die Koeffizienten auswirken, gemeint ist. In \ref{fig:wenigeMesswerte} stehen uns 25 Messpunkte zur Verfügung, in \ref{fig:vieleMesswerte} hingegen 100. Beide Messungen haben bei $x = 20$ einen Messwert, der sehr stark von den anderen Messwerten abweicht. Während sich bei \ref{fig:vieleMesswerte} durch die hohe Anzahl an Messpunkten eine gute Annäherung errechnen lässt, wird die Regressionsgerade bei \ref{fig:wenigeMesswerte} durch den abweichenden Wert sehr ungenau. \\
Um den Fehler durch stark abweichende Messwerte zu minimieren, kann man den Algorithmus nach Mosteller und Tukey aus dem Jahr 1977 \cite[339 \psqq]{Hoaglin.2000} verwenden und den Messwerten Gewichte zuweisen. Messwerte, die in einem erwartetem Bereich liegen, bekommen das Gewicht 1 zugewiesen, Ausreißer das Gewicht 0. Dies ist besonders bei der digitalen Verarbeitung von Daten sinnvoll. Durch mehrfaches Wiederholen dieses Vorgangs und weiterem Einschränken des erwartetem Bereichs, können die Koeffizienten genauer und zuverlässiger bestimmt werden.\\
\newpage
Der Bezug zum Fach Computational Engineering wird hier sehr deutlich. Oft ist eine große Menge Daten erhoben worden, die ausgewertet werden soll. Durch die Annäherung mit Hilfe kleinster Fehlerquadrate können versteckte Zusammenhänge zwischen Messpunkten erkannt werden, sowie der Verlauf der Funktion und unter Beachtung der Güte auch kommende Datenpunkte vorhergesagt werden.\\

