\section{Splines}\label{sec:splines}
	    Mit Hilfe der Polynominterpolation erhält man also geschlossene, differenzierbare Funktionen, die bei höheren Ordnungen jedoch zu starken Oszillationen neigen.
	    Eine weitere Möglichkeit aus Datenpunkten eine Funktion zu erhalten bieten die sogenannten Splines. Deren Methodik geht auf 
	    Variationsprobleme der Mechanik zurück und liefert oftmals brauchbarere Ergebnisse als die Polynominterpolation \cite[131 \psq]{Barwolff.2016}.
	    Wieder sind $(n+1)$ Datenpaare, sogenannte Knoten \cite[132]{Barwolff.2016} gegeben, durch die eine Funktion gefunden werden soll.\\
	    Eine Spline-Funktion $s_{k}(x)$ zu den Knoten $(x_{0},y_{0}),(x_{1},y_{1}),...,(x_{n},y_{n})$ vom Grad $ k \geq 1$ weist gemäß \cite[132]{Barwolff.2016} folgende Eigenschaften auf:\\
	    \begin{enumerate}
	    	\item[a)] $s_{k}(x)$ ist für $x \in [x_{0},x_{n}]$ stetig und $(k-1)$ mal stetig differenzierbar.\label{s3diff}
	    	\item[b)] $s_{k}(x)$ ist für $x \in [x_{i},x_{i+1}]$ und $i=0,1,...,n-1$ maximal ein Polynom vom Grad k.
	    \end{enumerate} 
        Werden für die gegebenen Knoten $x_{0},x_{1},...,x_{n}$ die Funktionswerte $y_{0},y_{1},...,y_{n}$ interpoliert gilt zusätzlich\\
        \begin{enumerate}
        	\item[c)] $s_{k}(x_{i})=y_{i}\qquad (i=0,1,...,n)$.
        \end{enumerate}
    \subsection{Lineare Splines}
        Wählt man $k=1$ erhält man sogenannte lineare Splines, eine stückweise lineare Funktion, deren Graph
        die Stützpunkte direkt verbindet \cite[132]{Barwolff.2016}.\\
        Die hier verwendete Matlab-Funktion \lstinline|myLSpline|(\textbf{x},\textbf{y}) erhält zwei Vektoren $\textbf{x}$ und $\textbf{y}$ mit gleicher Länge als Input.
        Der Vektor $\textbf{x}$ enthält in aufsteigender Reihenfolge alle Knoten, $\textbf{y}$ die entsprechenden Funktionswerte.\\
        
        \lstinputlisting[style=Matlab-editor,label={lst:linearCode}, caption={Inhalt der \texttt{myLSpline}-Funktion}, firstline=10, lastline=12]{data/myLSpline.m}
        Mit dem Code in \ref{lst:linearCode} werden jeweils zwei Knoten mittels einer linearen Funktion verbunden. In der ersten Zeile wird hierbei die Steigung berechnet, welche in 
        den nächsten beiden Zeilen mittels $y_{i}$ als Achsenabschnitt und $x_{i}$ als Verschiebung zum Aufstellen einer linearen Funktion im betrachteten Intervall
        verwendet wird. Zwecks Visualisierung \label{visualize} wurde die Sinus-Funktion an äquidistanten (Schrittweite 1) Knoten zwischen $-10$ und $10$ ausgewertet.\\
        \begin{figure}[h!]
        	\centering
        	\begin{subfigure}{.5\textwidth}
        		\centering
        		\includegraphics[width=\textwidth]{data/lSpline_10knots}
        		\caption{Linearer Spline mit 10 Knoten.}
        		\label{fig:lSpline10}
        	\end{subfigure}%
        	\begin{subfigure}{.5\textwidth}
        		\centering
        		\includegraphics[width=\textwidth]{data/lSpline_100knots}
        		\caption{Linearer Spline mit 100 Knoten.}
        		\label{fig:lSpline100}
        	\end{subfigure}
        	\caption{Linearer Spline der Sinus-Funktion bei unterschiedlicher Knotenanzahl.}
        	\label{fig:lSplineComparison}
        \end{figure}\\%
        %
    \subsection{Splines höherer Ordnung}
        Wie in Abbildung \ref{fig:lSplineComparison} zu erkennen ist, erhält man für höhere Knotenzahlen auch genauere Ergebnisse. 
        Jedoch ist weder \ref{fig:lSpline10} noch \ref{fig:lSpline100} in einem der Knoten stetig und damit auch nicht stetig differenzierbar. 
        \newpage
        Um dies zu Umgehen
        können Spline-Funktionen höherer Ordnung verwendet werden, die dann gemäß \ref{s3diff} a) $(k-1)$ mal stetig differenzierbar sind. Dies wird durch zusätzliche Bedingungen an die Ableitungen in den Knoten erreicht. \\
        In der Praxis relevant sind meist Spline-Funktionen der Ordnung $k \leq 3$ \cite[132 ]{Barwolff.2016}.
        Für den Fall $k=3$ erhält man zunächst die bis auf die reellen Parameter $\alpha,\beta,\gamma$ und $\delta$ bestimmte sogenannte kubische Spline-Funktion $s_{3}(x)$\cite[135]{Barwolff.2016}
        \begin{align}
        s_{i}(x)=\alpha_{i}+\beta_{i}(x-x_{i})+\gamma_{i}(x-x_{i})^{2}+\delta_{i}(x-x_{i})^{3}, \label{cubSpl} \\
        s_{i}:[x_{i},x_{i+1}]\rightarrow\mathbb{R}, \ i=0,...,n-1\text{.}
        \end{align}
        Für jedes Intervall $[x_{i}, x_{i+1}]$ erhält man mit \eqref{cubSpl} und $h_i=x_{i+1}-x_i$ \\
        \begin{align}
        s_{i}(x_{i})&=\alpha_{i}=y_{i}\label{first}\\
        s_{i}(x_{i+1})&=\alpha_{i}+\beta_{i}h_{i}x\gamma_{i}h_{i}^{2}+\delta_{i}h_{i}^{3}=y_{i+1}\\
        s_{i}^{\prime}(x_{i})&=\beta_{i}\\
        s_{i}^{\prime}(x_{i+1})&=\beta_{i}+2\gamma_{i}h_{i}+3\delta_{i}h_{i}^{2}\\
        s_{i}^{\prime \prime}(x_{i})&=2\gamma_{i}=m_{i}\\
        s_{i}^{\prime \prime}(x_{i+1})&=2\gamma_{i}+6\delta_{i}h_{i}=m_{i+1}.\label{last}
        \end{align}
        Die Werte $m_i$ stellen hierbei Hilfsgrößen dar, die die Ableitungen an den inneren Stützstellen vertreten.
        Aus den Gleichungen \eqref{first} bis \eqref{last} erhält man gemäß \cite[136]{Barwolff.2016} nach diversen Umformung 
        \begin{align}
        \alpha_{i}&:=y_i\\
        \beta_{i}&:=\frac{y_{i+1}-y_i}{h_i}-\frac{2m_i+m_{i+1}}{6}h_i\\
        \gamma_{i}&:=\frac{m_i}{2}\\
        \delta_{i}&:=\frac{m_{i+1}-m_i}{6h_i}.
        \end{align}
        Zusätzlich wird die Bedingung $p_{i-1}^{\prime}(x)=p_i^{\prime}(x)$ gestellt, was schließlich auf ein Gleichungssystem
        mit $(n-1)$ Gleichungen für $(n+1)$ Unbekannte führt. Dieses entsteht durch die Anforderungen an die Spline-Funktion
        im Inneren des Intervalls $[x_{0},x_{n}]$ stetige erste und zweite Ableitungen zu besitzen.\\
        Noch fehlen jedoch 2 Bedingungen.
        Diese werden allgemein durch verschieden wählbare Forderungen an das Verhalten der Funktion an den Rändern gewonnen, vgl. \cite[133 \psq]{Barwolff.2016}. Oft fordert man das 
        Verschwinden der zweiten Ableitung, also
        \begin{align}
        s_{3}(x_{0})^{\prime \prime}=s_{3}^{\prime \prime}(x_{n})=0. \label{Randbedingungen}
        \end{align}
        Insgesamt erhält man damit ein Gleichungssystem der Gestalt\\
        \\
        \begin{small}
        $\begin{pmatrix}
         2(h_{0}+h_{1}) & h_1            &    0   &  ...    & 0      \\
            h_1         & 2(h_{1}+h_{2}) &   h_2  & \ddots  & \vdots \\
             0          & h_2            & \ddots & \ddots  & 0      \\
            \vdots      & \ddots         & \ddots & \ddots  & h_{n-2}\\
             0          & ...            &    0   & h_{n-2} & 2(h_{n-2}+h_{n-1})
        \end{pmatrix}$
        $\begin{pmatrix}
        	m_1 \\
        	m_2 \\
        	\vdots \\
        	m_{n-2}\\
        	m_{n-1}
        \end{pmatrix}$
        $=$
        $\begin{pmatrix}
      	     \frac{6}{h_{1}} (y_{2}-y_{1})- \frac{6}{h_{0}} (y_{1}-y_{0})-h_{0}m_{0} \\
    	     \frac{6}{h_{2}} (y_3-y_2)- \frac{6}{h_1} (y_2-y_1) \\
    	     \vdots \\
    	     \frac{6}{h_{n-2}} (y_{n-1}-y_{n-2})- \frac{6}{h_{n-2}} (y_{n-2}-y_{n-3})\\
    	     \frac{6}{h_{n-1}} (y_n-y_{n-1})- \frac{6}{h_{n-1}} (y_{n-1}-y_{n-2})-h_{n-1}m_n
        \end{pmatrix}$.\\      
        \end{small}
        \\
        \\
        Meist wird die Matrix in Kurzschreibweise mit \textbf{S}, der Hilfsgrößenvektor als \textbf{m} und der Vektor der rechten Seit als \textbf{c} bezeichnet. Verwendet man die eben genannten Bedingungen \eqref{Randbedingungen} so ist $m_0=m_n=0$ zu beachten.
        In jeder Zeile der Matrix \textbf{S} ist der Diagonaleintrag größer als die jeweilige Summe der restlichen Einträge,
        man spricht von einer strikt diagonal dominanten Matrix. Hieraus kann die eindeutige Lösbarkeit des
        Gleichungssystem abgeleitet werden, was bedeutet, dass die kubischen Splines existieren und diese wiederum eindeutig sind, vgl. \cite[135]{Barwolff.2016}.\label{EindeutigkeitSpl}\\
        Wie in Abbildung \ref{fig:lSpline10} wird die Sinus-Funktion ausgewertet und die erhaltenen Datenpunkte jetzt mittels kubischer Splines approximiert.
        Hierbei wurde die bereits vorhandene Matlab-Funktion \lstinline|csapi(x,y,xx)| benutzt.
        
        \lstinputlisting[style = Matlab-editor, caption=Aufruf der \texttt{csapi}-Funktion,label =cSplCode, firstline=8, lastline=10]{data/cSpline.m} \label{cSplCode}
        
        Durch die Funktion wird für die selben in \ref{visualize} definierten Vektoren \textbf{x} und \textbf{y} 
        der entsprechende kubische Spline an den in \textbf{xx} definierten Punkten ausgewertet und diese als Vektor \textbf{yy} gespeichert.
        Dieser Vektor \textbf{yy} wird dann für den Plot genutzt.
         
        \begin{figure}[h!]
        	\centering
        	\begin{subfigure}{.5\textwidth}
        		\centering
        		\includegraphics[width=\textwidth]{data/lSpline_10knots}
        		\caption{Linearer Spline mit 10 Knoten aus \ref{fig:lSpline10}.}
        	\end{subfigure}%
        	\begin{subfigure}{.5\textwidth}
        		\centering
        		\includegraphics[width=\textwidth]{data/cSpline_10knots}
        		\caption{Kubischer Spline mit 10 Knoten.}
        		\label{fig:cSpline10}
        	\end{subfigure}
        	\caption{Vergleich von linearem und kubischem Spline bei gleicher Knotenanzahl.}
        	\label{fig:cSplineComparison}
        \end{figure}
    \noindent
        Wie in Abbildung \ref{fig:cSpline10} zu sehen ist, erhält man mit kubischen Splines selbst bei niedriger Knotenzahl brauchbare Ergebnisse.
        Insbesondere zeigt Abbildung \ref{fig:cSplineComparison}, dass bei gleicher Knotenanzahl ein kubischer Spline deutlich bessere Approximationen liefert
        als ein linearer. Zudem hat Abbildung \ref{fig:cSpline10} den Vorteil, dass der Graph zwei mal stetig differenzierbar ist und damit in vielen Fällen weitreichender verwendbar ist.\\
        \newpage
        \begin{figure}[h!]
        	\centering
        	\includegraphics[width=.5\textwidth]{data/CompcSpIntRunge}
        	\caption{Oszillationsverhalten von Interpolationspolynom und kubischem Spline.} 
            \label{fig:rungeSpline}
        \end{figure} 
        \noindent       
        In Abbildung \ref{fig:rungeSpline} wurde mittels der kubischen Spline-Funktion aus \ref{cSplCode} sowie dem Interpolationspolynom aus Abschnitt \ref{sec:lagr} versucht die Runge-Funktion aus Abschnitt \ref*{sec:runges} in der Form
        $f(x)=\frac{1}{1+10x^2}$ anzunähern. Es ist ein deutlicher Unterschied im Oszillationsverhalten festzustellen: Während der Spline nur minimalste Schwankungen aufweist, 
        vergrößern sich die Schwingungen des Interpolationspolynoms zum Rand des Intervalls hin deutlich. Zwar können die Oszillationen des Splines bei anderen Beispielen auch stärker ausfallen als hier,
        jedoch sind diese in jedem Fall weniger und sanfter ausgeprägt \cite[135]{Barwolff.2016}.\\ 
        Jedoch sei angemerkt, dass der Rechenaufwand und die Komplexität der Implementierung bei Splines zunehmender Ordnung deutlich steigt.
        Dieser Rechenaufwand in Verbindung mit den schon bei $n=3$ brauchbareren Ergebnissen führen dazu, dass kubische Splines in den meisten Fällen
        zur Anwendung kommen. Höhere Ordnungen führen zu teils gewaltigem Aufwand bei der Implementierung und zu langen Laufzeiten, während quadratische Splines nur geringfügig weniger 
        aufwendig sind, aber doch ungenauere Ergebnisse liefern.
        
        
     \subsection{Fehlerabschätzung der Spline-Interpolation}      
        Ebenso wie bei anderen Approximationsmethoden ist man bei der Spline-Interpolation daran interessiert den gemachten Fehler sinnvoll abzuschätzen.
        In Abschnitt \ref{EindeutigkeitSpl} wurde bereits auf die Eindeutigkeit der Lösung hingewiesen. Hinzu kommen nun folgende Behauptungen:\\
        Für alle $x\in [x_0,x_n]$ gelte mit\\
        \begin{align}
        M_n=\sup_{\xi \in [a,b]}|f^{n}(\xi)|
        \end{align} 
        für kubische Splines die Abschätzungen
        \begin{align*}
        |f(x)-s_3(x)|&\leq cM_4h^4\\
        |f(x)^{\prime}-s_3^{\prime}(x)|&\leq cM_4h^3\\
        |f(x)^{\prime \prime}-s_3^{\prime \prime}(x)|&\leq cM_4h^2\\
        |f(x)^{\prime \prime\prime}-s_3^{\prime \prime\prime}(x)|&\leq cM_4h\text{.}
        \end{align*}
        Allgemeiner gilt für Splines der Ordnung $n$\\
        \begin{align}
        |f^{(k)}(x)-s^{(k)}(x)|\leq cM_{n+1}h^{n+1-k}_{\max}.
        \end{align}
        Hierbei gelte zusätzlich mit $h_k=x_{k+1}-h_k$
        \begin{align*}
        h=max_{k=0,...,n-1}h_{k} ,\qquad c=\frac{h}{min_{k=0,...,n-1}h_k}\text{.}
        \end{align*}
        Zudem stellt $M_4>0$ eine Schranke für $|f^{4}(x)|$ auf $[x_0,x_n]$ dar. Analog zu \cite[Kapitel 5.5.1]{Barwolff.2016} sei auf den
        Beweis in \cite[1 \psqq]{Plato.2000} verwiesen, auf den aufgrund dessen hoher Komplexität hier nicht weiter eingegangen wird.\\
        Während man bei der Polynominterpolation durch Hinzunahme von Stützstellen oder Verkleinerung der Ordnung also starke Oszillationen in Kauf nehmen muss (vgl. \cite[1,\psqq]{Estep.2005}), kann
        bei kubischen Splines im Bereich großer vierter Ableitungen h verkleinert werden und damit die Genauigkeit erhöht werden, ohne solche Effekte zu erhalten \cite[139]{Barwolff.2016}.
     \subsection{Anwendungsbeispiel lineare Splines}
        Da lineare Splines nicht differenzierbar sind, ist ihre Anwendung oft begrenzt. Eine Situation, in der sie jedoch auch wegen ihrer Einfachheit gut zu verwenden sind ist, wenn die aus Datenpunkten zu erwartende Funktion vermutlich nicht
        differenzierbar sein wird. Es seien folgende Geschwindigkeitsdaten eines Fahrstuhls gegeben, der stets mit derselben Beschleunigung $|a|$ beschleunigt oder verzögert und dabei immer die Höchstgeschwindigkeit $1,5 \,\frac{m}{s}$ erreicht:\\
        \begin{table}[h!]
        	\centering
        \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
        	\hline
        	Zeitpunkt in Sekunden           & 0 & 2 & 4 & 6 & 8 & 10 & 12 & 14 & 16 & 18 & 20 & 22 & 24 & 26 & 28 & 30  \\
        	\hline
        	Geschwindigkeit in $\frac{m}{s}$& 0 & 0 & 1,5 & 1,5 & 0 & 0 & 0 & 1,5 & 0 & 0 & -1,5 & -1,5 & 0 & 0 & 1,5 & 0 \\
        	\hline        	
        \end{tabular}
        \caption{Erhobene Daten des Aufzugs.}
        \label{AufzugDaten}
        \end{table}
        Die gegebenen Werte werden nun mittels Interpolationspolynom und linearer bzw. kubischer Spline-Funktion angenähert.
        \begin{figure}[h!]
        	\centering
        	\begin{subfigure}{.45\textwidth}
        		\centering
        		\includegraphics[width=\textwidth]{data/elevatorCubic}
        		\caption{Kubische Spline-Funktion.}
        		\label{fig:elevatorGC}
        	\end{subfigure}%
        	\begin{subfigure}{.45\textwidth}
        		\centering
        		\includegraphics[width=\textwidth]{data/elevatorBad}
        		\caption{Interpolationspolynom vom Grad 15.}
        		\label{fig:elevatorB}
        	\end{subfigure}
        	\caption{Geschwindigkeitskurve des Aufzugs.}
        	\label{fig:elevatorComp}
        \end{figure}
        \begin{figure}[h!]
        		\centering
        		\includegraphics[width=.45\textwidth]{data/elevatorGood}
        		\caption{Lineare Spline-Funktion.}
        		\label{fig:elevatorG}
        \end{figure}
       	\newpage
        Wie in Abbildung \ref{fig:elevatorG} gut zu sehen ist, hat der lineare Spline ein sehr gutes Ergebnis geliefert. Das Interpolationspolynom hat, wie in Abschnitt \ref{sec:runges}
        bereits erläutert, durch die vielen äquidistanten Stützstellen ein starkes Oszillationsverhalten angenommen (man beachte den Maßstab). 
        Durch diese erreicht der Aufzug laut Abbildung \ref{fig:elevatorB} zwischenzeitlich Geschwindigkeiten von weit mehr als $150 \,\frac{m}{s}$ und erfährt in diesem Bereich zudem enorme 
        Beschleunigungen. Der kubische Spline weist deutlich geringere Oszillationen auf als das Interpolationspolynom, jedoch weichen die Werte vor allem in den Bereichen konstanter Geschwindigkeit ab. Sind Funktionen wie in Abbildung \ref{fig:elevatorG} zu erwarten, die nicht überall stetig sind und ggfs. auch Sprünge aufweisen, so kann die lineare Spline-Funktion durch die Messwerte deutlich bessere 
        Werte liefern als Polynome oder kubische Splines.