function b = calculateM(arr1,arr2)
b = (multipliedSum(arr1,arr2) - ((1/numel(arr1)) * sum(arr1(:)) * sum(arr2(:))))/( multipliedSum(arr1,arr1)-(1/numel(arr1))*sum(arr1(:))*sum(arr1(:)));
end
