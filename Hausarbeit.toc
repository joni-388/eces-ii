\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Selbstständigkeitserklärung}{2}{section.1}%
\contentsline {section}{\numberline {2}Abstract}{2}{section.2}%
\contentsline {section}{\numberline {3}Einführung}{3}{section.3}%
\contentsline {section}{\numberline {4}Hauptteil}{3}{section.4}%
\contentsline {subsection}{\numberline {4.1}Allgemeine Diskussion}{3}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Taylor-Approximation}{4}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Interpolation}{6}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Lagrange-Formel}{6}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Newtonsche Interpolationsformel}{7}{subsubsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.3}Fehlerabschätzung}{7}{subsubsection.4.3.3}%
\contentsline {subsubsection}{\numberline {4.3.4}Runges Phänomen}{8}{subsubsection.4.3.4}%
\contentsline {subsubsection}{\numberline {4.3.5}Chebyshev-Interpolation}{8}{subsubsection.4.3.5}%
\contentsline {subsubsection}{\numberline {4.3.6}Trigonometrische Interpolation}{9}{subsubsection.4.3.6}%
\contentsline {subsubsection}{\numberline {4.3.7}Anwendungen der Polynominterpolation}{11}{subsubsection.4.3.7}%
\contentsline {subsubsection}{\numberline {4.3.8}Beispiel: Beschleunigungskurve}{11}{subsubsection.4.3.8}%
\contentsline {subsection}{\numberline {4.4}Splines}{12}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}lineare Splines}{12}{subsubsection.4.4.1}%
\contentsline {subsubsection}{\numberline {4.4.2}Splines höherer Ordnung}{13}{subsubsection.4.4.2}%
\contentsline {subsubsection}{\numberline {4.4.3}Fehlerabschätzung der Spline-Interpolation}{15}{subsubsection.4.4.3}%
\contentsline {subsubsection}{\numberline {4.4.4}Anwendungsbeispiel lineare Splines}{15}{subsubsection.4.4.4}%
\contentsline {subsection}{\numberline {4.5}Fehlerquadrate}{16}{subsection.4.5}%
\contentsline {subsubsection}{\numberline {4.5.1}Lineare Regression}{17}{subsubsection.4.5.1}%
\contentsline {subsubsection}{\numberline {4.5.2}Polynomiale Regression}{17}{subsubsection.4.5.2}%
\contentsline {subsubsection}{\numberline {4.5.3}Beispiel: Ohmsche Gesetz}{18}{subsubsection.4.5.3}%
\contentsline {section}{\numberline {5}Schluss/Fazit}{20}{section.5}%
\contentsline {section}{\numberline {6}Anhang}{21}{section.6}%
